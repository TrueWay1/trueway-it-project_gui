// Flavio Heim, Damian Schumacher, Nicolai Zaccariello
package org.fhnw.trueway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;


// TODO: https://spring.io/guides/gs/serving-web-content/
// FeignClient: https://www.baeldung.com/spring-cloud-openfeign
// Thymeleaf Security: https://www.thymeleaf.org/doc/articles/springsecurity.html
@SpringBootApplication
@EnableFeignClients
public class TruewayGuiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TruewayGuiApplication.class, args);
	}

}
