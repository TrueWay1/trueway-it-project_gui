// Damian Schumacher
package org.fhnw.trueway.web.controller.api.controller;

import org.fhnw.trueway.infra.client.ImageClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Damian Schumacher
 */
@RestController
@RequestMapping("api")
public class ImageController {
    private static final Logger LOG = LoggerFactory.getLogger(ImageController.class);

    private final ImageClient client;

    public ImageController(ImageClient client) {
        this.client = client;
    }


    @GetMapping(value = "/image/{code}", produces = MediaType.IMAGE_JPEG_VALUE)
    public byte[] findImageByCode(@PathVariable String code) {
        LOG.info("ENTER: ImageController#findImageByCode(" + code + ")");
        return client.getImageByCode(code);
    }
}
