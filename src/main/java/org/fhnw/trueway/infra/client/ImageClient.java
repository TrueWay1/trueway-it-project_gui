// Damian Schumacher
package org.fhnw.trueway.infra.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "image", url = "localhost:8080")
public interface ImageClient {

    @GetMapping(value = "/image/{code}", consumes = MediaType.IMAGE_JPEG_VALUE)
    byte[] getImageByCode(@PathVariable String code);
}
