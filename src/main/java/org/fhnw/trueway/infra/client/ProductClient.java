// Damian Schumacher
package org.fhnw.trueway.infra.client;

import org.openapitools.model.ProductDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

import java.util.List;

@FeignClient(name = "product", url = "localhost:8080")
public interface ProductClient {

    @GetMapping("/product")
    List<ProductDto> getProducts();

    @GetMapping("/product/{id}")
    ProductDto getProduct(@PathVariable Long id);

    @PostMapping("/product")
    ProductDto postProduct(ProductDto dto);

    @PutMapping("/product/{id}")
    ProductDto putProduct(@PathVariable Long id, ProductDto dto);
}

