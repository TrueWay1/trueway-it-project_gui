// Damian Schumacher
package org.fhnw.trueway.infra.client;

import org.openapitools.model.PostShoppingCartRequest;
import org.openapitools.model.ShoppingCartDto;
import org.openapitools.model.UserDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "user", url = "localhost:8080")
public interface UserClient {

    @GetMapping("/user/email/{email}")
    UserDto getUserByEmail(@PathVariable("email") String email);

    @PostMapping("/user/{id}/shoppingcart")
    ShoppingCartDto postUserShoppingCartById(
            @PathVariable("id") Long id,
            @RequestBody PostShoppingCartRequest request
    );

    @PostMapping("/user")
    UserDto postUser(UserDto dto);

   /* @PutMapping("/account")
    UserDto putUser(@PathVariable ("email") String email, UserDto dto);
**/
}