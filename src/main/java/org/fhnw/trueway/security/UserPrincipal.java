// Flavio Heim
package org.fhnw.trueway.security;
import org.openapitools.model.UserDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;

import static java.lang.Boolean.TRUE;

public class UserPrincipal implements UserDetails {
	private static final long serialVersionUID = 1L;

	private static Logger LOG = LoggerFactory.getLogger(UserPrincipal.class);

	private UserDto user;

	public UserPrincipal(UserDto user) {
        this.user = user;
    }

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		LOG.trace("ENTER: UserPrincipal#getAuthorities()");
		return Collections.emptyList();
	}

	@Override
	public String getPassword() {
		LOG.trace("ENTER: UserPrincipal#getPassword()");
		return user.getPassword();
	}

	@Override
	public String getUsername() {
		LOG.trace("ENTER: UserPrincipal#getUsername()");
		return user.getEmail();
	}

	@Override
	public boolean isAccountNonExpired() {
		LOG.trace("ENTER: UserPrincipal#isAccountNonExpired()");
		return TRUE;
	}

	@Override
	public boolean isAccountNonLocked() {
		LOG.trace("ENTER: UserPrincipal#isAccountNonLocked()");
		return TRUE;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		LOG.trace("ENTER: UserPrincipal#isCredentialsNonExpired()");
		return TRUE;
	}

	@Override
	public boolean isEnabled() {
		LOG.trace("ENTER: UserPrincipal#isEnabled()");
		return TRUE;
	}

}
