// Flavio Heim
package org.fhnw.trueway.security;

import org.fhnw.trueway.infra.client.UserClient;
import org.openapitools.model.UserDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserPrincipalService implements UserDetailsService {
	private static final Logger LOG = LoggerFactory.getLogger(UserPrincipalService.class);
	private final UserClient client;


	public UserPrincipalService(UserClient client) {
		this.client = client;
	}

	@Override
	public UserDetails loadUserByUsername(String email) {
		LOG.debug("ENTER: UserPrincipalService#loadUserByUsername(" + email + ")");

		UserDto user = client.getUserByEmail(email);

		if (user == null) {
			throw new UsernameNotFoundException(email);
		}

		return new UserPrincipal(user);
	}
}
