// Damian Schumacher
package org.fhnw.trueway.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomePageController {

    private static final Logger LOG = LoggerFactory.getLogger(HomePageController.class);

    @GetMapping({"/", "/homepage"})
    public String getHome() {
        LOG.info("ENTER: HomePageController#getHome()");
        return "homepage";
    }
}
