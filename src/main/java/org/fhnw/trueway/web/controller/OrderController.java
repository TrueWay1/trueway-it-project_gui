// Damian Schumacher
package org.fhnw.trueway.web.controller;

import org.fhnw.trueway.infra.client.ProductClient;
import org.fhnw.trueway.infra.client.UserClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;

@Controller
@RequestMapping("order")
public class OrderController {
    private static final Logger LOG = LoggerFactory.getLogger(OrderController.class);
    private final UserClient userClient;
    private final ProductClient productClient;

    public OrderController(UserClient userClient, ProductClient productClient) {
        this.userClient = userClient;
        this.productClient = productClient;
    }

    /**
     * Shows shopping cart by principal
     * @param principal the user logged in
     * @return ModelAndView the shopping cart page
     */
    @GetMapping
    public ModelAndView getShoppingCart(Principal principal) {
        LOG.info("ENTER: OrderController#getShoppingCart(" + principal + ")");

        ModelAndView mv = new ModelAndView( "shoppingcart/order");
        mv.addObject("user", userClient.getUserByEmail(principal.getName()));
        return mv;
    }

}

