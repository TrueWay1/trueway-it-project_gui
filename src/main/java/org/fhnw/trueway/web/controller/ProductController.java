// Nicolai Zaccariello
package org.fhnw.trueway.web.controller;

import org.fhnw.trueway.infra.client.ProductClient;
import org.openapitools.model.ProductDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
@RequestMapping("product")
public class ProductController {
    private static final Logger LOG = LoggerFactory.getLogger(ProductController.class);
    private final ProductClient client;

    public ProductController(ProductClient client) {
        this.client = client;
    }

    @GetMapping
    public ModelAndView getProducts() {
        LOG.info("ENTER: ProductController#getProducts()");
        ModelAndView mv = new ModelAndView("product/products");
        mv.addObject("products", client.getProducts());
        return mv;
    }

    @GetMapping("/{id}")
    public ModelAndView getProductDetailById(@PathVariable Long id) {
        LOG.info("ENTER: ProductController#getProductDetailById(" + id + ")");
        ModelAndView mv = new ModelAndView("product/product-detail");
        mv.addObject("product", client.getProduct(id));
        return mv;
    }
    @GetMapping("/list")
    public ModelAndView getProductList() {
        LOG.info("ENTER: ProductController#getProductList()");
        ModelAndView mv = new ModelAndView("product/list.html");
        mv.addObject("products", client.getProducts());
        return mv;
    }

    @GetMapping("/view/{id}")
    public ModelAndView getProductView(@PathVariable Long id) {
        LOG.info("ENTER: ProductController#getProductView(" + id +")");
        ModelAndView mv = new ModelAndView("product/view.html");
        mv.addObject("product", client.getProduct(id));
        return mv;
    }

    @GetMapping("/edit/{id}")
    public ModelAndView getProductEdit(@PathVariable Long id) {
        LOG.info("ENTER: ProductController#getProductEdit(" + id +")");
        ModelAndView mv = new ModelAndView("product/edit.html");
        mv.addObject("product", client.getProduct(id));
        return mv;
    }

    @PostMapping("/edit")
    public ModelAndView postProductSave(
            @ModelAttribute("product") @Valid ProductDto formProduct,
            BindingResult errors
    ) {
        LOG.info("ENTER: ProductController#postProductSave(" + formProduct +")");

        if (!errors.hasErrors()) {
            Long id = formProduct.getId();
            if (null == id) {
                client.postProduct(formProduct);
            } else {
                client.putProduct(id, formProduct);
            }
            return new ModelAndView("redirect:/product/list");

        } else {
            // Handle Error
            LOG.warn("Validation error: " + errors);
            ModelAndView mv = new ModelAndView("product/edit");
            mv.addObject("errors", errors);
            mv.addObject("product", formProduct);
            return mv;
        }
    }
}
