// Flavio Heim
package org.fhnw.trueway.web.controller;

import org.fhnw.trueway.infra.client.UserClient;
import org.openapitools.model.UserDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
@RequestMapping("account")
public class RegisterController {
    private static final Logger LOG = LoggerFactory.getLogger(RegisterController.class);

    public final UserClient client;

    public RegisterController(UserClient client) {
        this.client = client;
    }

    @GetMapping("/accountregister")
    public String getRegister() {
        LOG.info("ENTER: RegisterController#getRegister()");
        return "account/accountregister";
    }


    /**
     * Fuer neue User Registierung, bitte für Änderng eine PutMapping
     * @param formUser
     * @param errors
     * @return
     */
    @PostMapping("/accountregister")
    public ModelAndView postRegisterSave(
        @ModelAttribute("user") @Valid UserDto formUser,
        BindingResult errors
        ) { LOG.info("ENTER: RegisterController#postRegisterSave(" + formUser +")");
        if (!errors.hasErrors()) {
            client.postUser(formUser);
            return new ModelAndView("redirect:/login");
        } else {
            ModelAndView mv = new ModelAndView("account/accountregister");
            mv.addObject("errors, errors");
            mv.addObject("user", formUser);
            return mv;
        }
    }


}
