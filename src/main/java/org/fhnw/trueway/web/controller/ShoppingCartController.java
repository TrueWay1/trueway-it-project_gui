// Nicolai Zaccariello
package org.fhnw.trueway.web.controller;

import org.fhnw.trueway.infra.client.ProductClient;
import org.fhnw.trueway.infra.client.UserClient;
import org.openapitools.model.PostShoppingCartRequest;
import org.openapitools.model.ShoppingCartDto;
import org.openapitools.model.UserDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;

@Controller
@RequestMapping("shoppingcart")
public class ShoppingCartController {
    private static final Logger LOG = LoggerFactory.getLogger(RegisterController.class);
    private final UserClient userClient;
    private final ProductClient productClient;

    public ShoppingCartController(UserClient userClient, ProductClient productClient) {
        this.userClient = userClient;
        this.productClient = productClient;
    }


    /**
     * Shows shopping cart by principal
     * @param principal the user logged in
     * @return ModelAndView the shopping cart page
     */
    @GetMapping("/shoppingcart")
    public ModelAndView getShoppingCart(Principal principal) {
        LOG.info("ENTER: ShoppingCartController#getShoppingCart(" + principal + ")");
        ModelAndView mv = new ModelAndView( "shoppingcart/shoppingcart");
        mv.addObject("shoppingCart", findShoppingCartByUserName(principal.getName()));
        return mv;
    }

    private ShoppingCartDto findShoppingCartByUserName(String email) {
        UserDto user = userClient.getUserByEmail(email);
        return user.getShoppingCart();
    }

    @PostMapping("/shoppingcart")
    public ModelAndView postShoppingCart(Principal principal, PostShoppingCartRequest request) {
        LOG.info("ENTER: ShoppingCartController#postShoppingCart(" + principal + "," + request + ")");
        UserDto user = userClient.getUserByEmail(principal.getName());
        userClient.postUserShoppingCartById(user.getId(), request);
        return new ModelAndView("redirect:/shoppingcart/shoppingcart");
    }


}
